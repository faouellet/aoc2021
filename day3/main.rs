use std::{
    error::Error,
    fs::File, 
    io::{BufRead, BufReader},
    result::Result,
};

pub fn get_most_common_bit(contents: &Vec<String>, indexes: &Vec<usize>) -> Vec<char> {
    debug_assert!(!contents.is_empty());
    let mut counts : Vec<u32> = vec![0; contents[0].len()];
    for idx in indexes {
        let line = &contents[*idx];
        for (idx, value) in line.chars().enumerate() {
            counts[idx] += value.to_digit(10).unwrap();
        }
    }

    let indexes_length = indexes.len() as u32;
    let mut most_common = Vec::new();
    counts.iter().for_each(|&count| {
        most_common.push(if indexes_length - count <= count { '1' } else { '0' });
    });

    most_common
}

fn main() -> Result<(), Box<dyn Error>> {
    let file = File::open("day3/input")?;
    let buf = BufReader::new(file);
    let contents = buf.lines().collect::<Result<Vec<_>,_>>()?;

    debug_assert!(!contents.is_empty());
    debug_assert!(contents.windows(2).all(|window| window[0].len() == window[1].len()));

    let indexes = (0..contents.len()).collect();
    let mut most_common = get_most_common_bit(&contents, &indexes);

    let gamma = u32::from_str_radix(&most_common.iter().collect::<String>(), 2)?;
    let mask = (1 << most_common.len()) - 1;
    println!("Part 1 answer: {}", gamma * (!gamma & mask));

    let mut idx_left : Vec<usize> = (0..contents.len()).collect();
    let mut i_bit = 0;
    while idx_left.len() > 1 && i_bit < most_common.len() {
        idx_left = idx_left.iter().filter(|idx| contents[**idx].chars().nth(i_bit).unwrap() == most_common[i_bit]).cloned().collect();
        most_common = get_most_common_bit(&contents, &idx_left);
        i_bit += 1;
    }
    debug_assert!(idx_left.len() == 1);
    let oxygen_scrubber_rating = u32::from_str_radix(&contents[idx_left[0]],2)?;

    idx_left = (0..contents.len()).collect();
    i_bit = 0;
    while idx_left.len() > 1 && i_bit < most_common.len() {
        idx_left = idx_left.iter().filter(|idx| contents[**idx].chars().nth(i_bit).unwrap() != most_common[i_bit]).cloned().collect();
        most_common = get_most_common_bit(&contents, &idx_left);
        i_bit += 1;
    }
    debug_assert!(idx_left.len() == 1);
    let co2_scrubber_rating = u32::from_str_radix(&contents[idx_left[0]],2)?;

    println!("Part 2 answer: {}", oxygen_scrubber_rating * co2_scrubber_rating);

    Ok(())
}
