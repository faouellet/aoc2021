use std::{
    error::Error,
    fs::File, 
    io::{BufRead, BufReader},
    result::Result,
};

fn main() -> Result<(), Box<dyn Error>> {
    let file = File::open("day4/input")?;
    let mut reader = BufReader::new(file);
    let mut buf = String::new();
    reader.read_line(&mut buf)?;
    let numbers : Vec<u32> = buf.split(',').filter_map(|x| x.trim().parse().ok()).collect();

    reader.read_line(&mut buf)?; // Empty line

    let mut boards = Vec::new();
    boards.push(Vec::new());
    let mut current_board_idx = 0;
    loop {
        buf.clear();
        match reader.read_line(&mut buf) {
            Ok(bytes_read) => {
                println!("{}", buf.len());
                if bytes_read == 0 {
                    break; // We're done
                }
                else if buf.len() == 1 {
                    current_board_idx += 1;
                    boards.push(Vec::new());
                }
                else {
                    let mut line_numbers : Vec<(u32, u8)> = buf.split_whitespace().filter_map(|x| Some((x.parse().ok()?, 0))).collect();
                    boards[current_board_idx].append(&mut line_numbers);
                }
            }
            Err(err) => panic!("{}", err),
        }
    }

    //let numbers = vec![7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1];
    //let mut boards = Vec::new();
    //boards.push(vec![(22,0),(13,0),(17,0),(11,0),(0,0),(8,0),(2,0),(23,0),(4,0),(24,0),(21,0),(9,0),(14,0),(16,0),(7,0),(6,0),(10,0),(3,0),(18,0),(5,0),(1,0),(12,0),(20,0),(15,0),(19,0)]);
    //boards.push(vec![(3,0),(15,0),(0,0),(2,0),(22,0),(9,0),(18,0),(13,0),(17,0),(5,0),(19,0),(8,0),(7,0),(25,0),(23,0),(20,0),(11,0),(10,0),(24,0),(4,0),(14,0),(21,0),(16,0),(12,0),(6,0)]);
    //boards.push(vec![(14,0),(21,0),(17,0),(24,0),(4,0),(10,0),(16,0),(15,0),(9,0),(19,0),(18,0),(8,0),(23,0),(26,0),(20,0),(22,0),(11,0),(13,0),(6,0),(5,0),(2,0),(0,0),(12,0),(3,0),(7,0)]);

    let mut last_winning_board_idx = 0;
    let mut last_winning_nb_idx = 0;
    let mut board_indices : Vec<usize> = (0..boards.len()).collect();
    for i_nb in 0..numbers.len() {
        println!("*********************************");
        println!("NUMBER: {}", numbers[i_nb]);

        let current_nb = &numbers[i_nb];
        let mut winning_boards = Vec::new();
        for i_board in &board_indices {
            let current_board = &mut boards[*i_board];
            if let Some(nb_pos) = current_board.iter().position(|x| x.0 == *current_nb) {
                current_board[nb_pos].1 = 1;
                let has_complete_line = current_board.chunks(5).any(|chunk| chunk.iter().all(|x| x.1 == 1));
                let mut has_complete_column = false;
                for i in 0..5 {
                    has_complete_column |= current_board.iter().skip(i).step_by(5).all(|x| x.1 == 1);
                }
                if has_complete_line || has_complete_column {
                    winning_boards.push(i_board);
                    last_winning_nb_idx = i_nb;
                    last_winning_board_idx = *i_board;
                }
            }
        }

        board_indices = board_indices.iter().filter(|x| !winning_boards.iter().position(|y| y == x).is_some()).cloned().collect();

        if board_indices.is_empty() {
            break;
        }
    }

    let score : u32 = boards[last_winning_board_idx].iter().fold(0, |acc, x| if x.1 == 0 { acc + x.0 } else { acc }) * numbers[last_winning_nb_idx];
    println!("Answer: {}", score);

    Ok(())
}

