use std::{
    error::Error,
    fs::File, 
    io::{BufRead, BufReader},
    result::Result,
};

fn main() -> Result<(), Box<dyn Error>> {
    let file = File::open("day2/input")?;
    let buf = BufReader::new(file);
    let commands = buf.lines().map(|line| -> Result<(String, i32), Box<dyn Error>> {
        let line_value = line?;
        let split_index = line_value.find(' ').ok_or("Split index not found")?;
        let (dir, unit) = line_value.split_at(split_index);
        let unit_value = unit.trim().parse::<i32>()?;
        Ok((dir.to_owned(), unit_value))
    }).collect::<Result<Vec<_>, _>>()?;

    let position1 = commands.iter().try_fold((0, 0), |(horizontal, depth), command| -> Result<(i32, i32), Box<dyn Error>> {
        let (dir, unit_value) = command;
        match dir.as_str() {
            "forward" => Ok((horizontal + unit_value, depth)),
            "up" => Ok((horizontal, depth - unit_value)),
            "down" => Ok((horizontal, depth + unit_value)),
            _ => Err(format!("Unknown direction: {}", &dir).into())
        }
    })?;
    println!("Part 1 answer: {}", position1.0 * position1.1);
    let position2 = commands.iter().try_fold((0, 0, 0), |(horizontal, depth, aim), command| -> Result<(i32, i32, i32), Box<dyn Error>> {
        let (dir, unit_value) = command;
        match dir.as_str() {
            "forward" => Ok((horizontal + unit_value, depth + aim * unit_value, aim)),
            "up" => Ok((horizontal, depth, aim - unit_value)),
            "down" => Ok((horizontal, depth, aim + unit_value)),
            &_ => Err(format!("Unknown direction: {}", &dir).into())
        }
    })?;
    println!("Part 2 answer: {}", position2.0 * position2.1);
    Ok(())
}
