#![feature(iter_zip)]
use std::{
    error::Error,
    fs::File, 
    io::{BufRead, BufReader},
    iter::zip,
    result::Result,
};

fn main() -> Result<(), Box<dyn Error>> {
    let file = File::open("day1/input")?;
    let buf = BufReader::new(file);
    let contents : Vec<i32> = buf.lines().map(|line| -> Result<i32, Box<dyn Error>> { Ok(line?.parse::<i32>()?) }).collect::<Result<Vec<_>,_>>()?;
    //let contents = vec![199, 200, 208, 210, 200, 207, 240, 269, 260, 263];

    let sum = zip(&contents, &contents[1..]).filter(|(lhs, rhs)| lhs < rhs).count();
    println!("Part 1 answer: {}", sum);

    let windowed_contents : Vec<i32> = contents.windows(3).map(|window| window.iter().sum()).collect();
    let windowe_sum = zip(&windowed_contents, &windowed_contents[1..]).filter(|(lhs, rhs)| lhs < rhs).count();
    println!("Part 2 answer: {}", windowe_sum);

    Ok(())
}
